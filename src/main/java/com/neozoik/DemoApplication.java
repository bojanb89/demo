package com.neozoik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication(exclude = { HypermediaAutoConfiguration.class })
public class DemoApplication extends SpringBootServletInitializer{

	  // this is needed for war packaging of the applicaiton
	  @Override
	  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    return application.sources(DemoApplication.class);
	  }
	  
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
